const http = require('http');
const port = process.env.PORT || 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  const msg = 'Hello Node!\ndb_server: ' + process.env.db_server + '\ndb_username: ' + process.env.db_username + '\ndb_password: ' + process.env.db_password
  res.end(msg);
});

server.listen(port, () => {
  console.log(`Server running on http://localhost:${port}/`);
  // console.log(process.env);
});
