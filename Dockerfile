FROM node:18-alpine

ENV db_server=""
ENV db_username=""
ENV db_password=""

WORKDIR /app

COPY . /

RUN JOBS=MAX npm install --production --unsafe-perm && \
npm cache verify && \
rm -rf /tmp/*

EXPOSE 3000

CMD ["npm", "start"]

# Metadata
LABEL org.opencontainers.image.vendor="Matt Shields" \
	org.opencontainers.image.url="https://gitlab.com/mattshields5/nodejs-example-app" \
	org.opencontainers.image.title="Nodejs Example App" \
	org.opencontainers.image.description="A sample Nodejs and flask docker app" \
	org.opencontainers.image.version="v1.0.0" \
	org.opencontainers.image.documentation="https://gitlab.com/mattshields5/nodejs-example-app"
